# A1200OkiRTC

This project is a design of a real-time clock module for the Amiga 1200 clock port based on the OKI MSM6242 or EPSON 72421.

## Pictures

The picture below shows Revision1 of the board. The initial version was missing 3 traces which I added manually. The files in this repository have the corrections, though.
![installed in A1200](https://gitlab.com/HenrykRichter/a1200okirtc/raw/master/Pics/A1200okiled_installed.jpg)


## Parts list
- 1 RTC OKI MSM6242 DIP (or EPSON 72421 DIP)
- 1 Diode BAS40-05 SOT-23
- 3 Resistors 10k 0805
- 2 Capacitors 22pF 0805
- 1 Crystal 32768 Hz (around 5x2mm, e.g. Citizen tuning fork CM519-32.768KDZF-UT)
- 1 Capacitor 100nF 0805
- 1 Capacitor 4.7uF 0805
- 1 Socket Strip RM2.00 2x11 positions
- 1 CR2032 battery holder THT

## Building and installation notes
Soldering should be straightforward for the SMD components. The socket strip J1 and the Oki chip are also populated on the component side.
C1,C2 and Y1 can be left unpopulated in case that the EPSON 72421 is used as RTC chip.

Only the CR2032 holder is soldered to the opposite side of the PCB such that it points upward when the module is mounted to the A1200. You may need to trim some of the RTC pins for flush mounting of the CR2032 holder.

The finished board should only fit in one direction in the A1200, pointing towards the rear and away from the Kickstart ROMs. Please note that other RTC solutions might have a different orientation.


