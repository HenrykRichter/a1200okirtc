EESchema Schematic File Version 5
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
Comment5 ""
Comment6 ""
Comment7 ""
Comment8 ""
Comment9 ""
$EndDescr
$Comp
L Connector_Generic:Conn_02x11_Odd_Even J1
U 1 1 5E223DEB
P 1450 1550
F 0 "J1" H 1500 2267 50  0000 C CNN
F 1 "Clock Port" H 1500 2176 50  0000 C CNN
F 2 "Connector_PinSocket_2.00mm:PinSocket_2x11_P2.00mm_Vertical" H 1450 1550 50  0001 C CNN
F 3 "~" H 1450 1550 50  0001 C CNN
	1    1450 1550
	1    0    0    -1  
$EndComp
Wire Wire Line
	1750 1050 2000 1050
Wire Wire Line
	1750 1150 2000 1150
Wire Wire Line
	2000 1950 1750 1950
Wire Wire Line
	2000 1250 1750 1250
Wire Wire Line
	2000 1750 1750 1750
Wire Wire Line
	2000 1450 1750 1450
Wire Wire Line
	2000 1550 1750 1550
Wire Wire Line
	2000 2050 1750 2050
Wire Wire Line
	2000 1650 1750 1650
Wire Wire Line
	2000 1350 1750 1350
Wire Wire Line
	2000 1850 1750 1850
Text Label 875  1050 0    50   ~ 0
GND
Text Label 1800 1050 0    50   ~ 0
+5V
Text Label 875  1150 0    50   ~ 0
{slash}INT6
Text Label 875  1250 0    50   ~ 0
{slash}RTC_CS
Wire Wire Line
	800  1250 1250 1250
Wire Wire Line
	800  1150 1250 1150
Wire Wire Line
	800  1050 1250 1050
Wire Wire Line
	800  1350 1250 1350
Wire Wire Line
	800  1450 1250 1450
Text Label 875  1350 0    50   ~ 0
{slash}IORD
Text Label 900  1450 0    50   ~ 0
A5
Wire Wire Line
	800  1550 1250 1550
Wire Wire Line
	800  1650 1250 1650
Wire Wire Line
	800  1750 1250 1750
Wire Wire Line
	800  1850 1250 1850
Wire Wire Line
	800  1950 1250 1950
Wire Wire Line
	800  2050 1250 2050
Text Label 1825 1450 0    50   ~ 0
A4
Text Label 900  1550 0    50   ~ 0
A3
Text Label 1825 1550 0    50   ~ 0
A2
Text Label 900  1650 0    50   ~ 0
D23
Text Label 900  1750 0    50   ~ 0
D21
Text Label 900  1850 0    50   ~ 0
D19
Text Label 900  1950 0    50   ~ 0
D17
Text Label 900  2050 0    50   ~ 0
GND
Text Label 1825 1650 0    50   ~ 0
D22
Text Label 1825 1750 0    50   ~ 0
D20
Text Label 1825 1850 0    50   ~ 0
D18
Text Label 1825 1950 0    50   ~ 0
D16
Text Label 1825 2050 0    50   ~ 0
{slash}RESET
Text Label 1800 1150 0    50   ~ 0
{slash}SPARE_CS
Text Label 1800 1250 0    50   ~ 0
{slash}RTC_DS
Text Label 1800 1350 0    50   ~ 0
{slash}IOWR
$Comp
L Device:Battery_Cell BT1
U 1 1 5E2296FE
P 1675 2825
F 0 "BT1" H 1793 2921 50  0000 L CNN
F 1 "CR2032" H 1793 2830 50  0000 L CNN
F 2 "Battery:BatteryHolder_Keystone_103_1x20mm" V 1675 2885 50  0001 C CNN
F 3 "~" V 1675 2885 50  0001 C CNN
	1    1675 2825
	1    0    0    -1  
$EndComp
$Comp
L Diode:BAV70 D1
U 1 1 5E22BA5D
P 2375 2625
F 0 "D1" H 2375 2841 50  0000 C CNN
F 1 "BAS40-05" H 2375 2750 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:SOT-23" H 2375 2625 50  0001 C CNN
F 3 "https://assets.nexperia.com/documents/data-sheet/BAV70_SER.pdf" H 2375 2625 50  0001 C CNN
	1    2375 2625
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0101
U 1 1 5E230B39
P 1375 2925
F 0 "#PWR0101" H 1375 2675 50  0001 C CNN
F 1 "GND" H 1380 2752 50  0000 C CNN
F 2 "" H 1375 2925 50  0001 C CNN
F 3 "" H 1375 2925 50  0001 C CNN
	1    1375 2925
	1    0    0    -1  
$EndComp
Wire Wire Line
	2075 2625 1675 2625
Wire Wire Line
	2675 2625 2875 2625
Text Label 2700 2625 0    50   ~ 0
+5V
Wire Wire Line
	2375 2850 2375 2825
$Comp
L amiga-chips:OKIMSM6242 U1
U 1 1 5E233A31
P 5850 1525
F 0 "U1" H 5850 2391 50  0000 C CNN
F 1 "OKIMSM6242" H 5850 2300 50  0000 C CNN
F 2 "Package_DIP:DIP-18_W7.62mm" H 5850 1525 50  0001 C CNN
F 3 "" H 5850 1525 50  0001 C CNN
	1    5850 1525
	1    0    0    -1  
$EndComp
Wire Wire Line
	4975 975  5300 975 
Wire Wire Line
	5300 1075 4975 1075
Wire Wire Line
	5300 1175 4975 1175
Wire Wire Line
	5300 1275 4975 1275
Text Label 5025 975  0    50   ~ 0
D19
Text Label 5025 1075 0    50   ~ 0
D18
Text Label 5025 1175 0    50   ~ 0
D17
Text Label 5025 1275 0    50   ~ 0
D16
Wire Wire Line
	5300 1425 4975 1425
Wire Wire Line
	5300 1525 4975 1525
Wire Wire Line
	5300 1625 4975 1625
Wire Wire Line
	5300 1725 4975 1725
Text Label 5050 1425 0    50   ~ 0
A5
Text Label 5050 1525 0    50   ~ 0
A4
Text Label 5050 1625 0    50   ~ 0
A3
Text Label 5050 1725 0    50   ~ 0
A2
Wire Wire Line
	5850 2425 5850 2650
$Comp
L power:GND #PWR0102
U 1 1 5E235D9E
P 5850 2650
F 0 "#PWR0102" H 5850 2400 50  0001 C CNN
F 1 "GND" H 5855 2477 50  0000 C CNN
F 2 "" H 5850 2650 50  0001 C CNN
F 3 "" H 5850 2650 50  0001 C CNN
	1    5850 2650
	1    0    0    -1  
$EndComp
Wire Wire Line
	2375 2850 2875 2850
Text Label 2600 2850 0    50   ~ 0
RTCVCC
Wire Wire Line
	5850 875  5850 800 
Wire Wire Line
	5850 800  5025 800 
Text Label 5075 800  0    50   ~ 0
RTCVCC
$Comp
L Device:Crystal Y1
U 1 1 5E2377E2
P 6725 2025
F 0 "Y1" V 6679 2156 50  0000 L CNN
F 1 "32768Hz" V 6770 2156 50  0000 L CNN
F 2 "Crystal:Crystal_SMD_MicroCrystal_CC4V-T1A-2Pin_5.0x1.9mm_HandSoldering" H 6725 2025 50  0001 C CNN
F 3 "~" H 6725 2025 50  0001 C CNN
	1    6725 2025
	0    1    1    0   
$EndComp
$Comp
L Device:C C1
U 1 1 5E23894D
P 7225 1875
F 0 "C1" V 6973 1875 50  0000 C CNN
F 1 "22pF" V 7064 1875 50  0000 C CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 7263 1725 50  0001 C CNN
F 3 "~" H 7225 1875 50  0001 C CNN
	1    7225 1875
	0    1    1    0   
$EndComp
$Comp
L Device:C C2
U 1 1 5E23CBFC
P 7225 2175
F 0 "C2" H 7340 2221 50  0000 L CNN
F 1 "22pF" H 7340 2130 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 7263 2025 50  0001 C CNN
F 3 "~" H 7225 2175 50  0001 C CNN
	1    7225 2175
	0    1    1    0   
$EndComp
Wire Wire Line
	6400 1875 6725 1875
Wire Wire Line
	6725 1875 7075 1875
Connection ~ 6725 1875
Wire Wire Line
	6400 2125 6550 2125
Wire Wire Line
	6550 2125 6550 2175
Wire Wire Line
	6550 2175 6725 2175
Wire Wire Line
	6725 2175 7075 2175
Connection ~ 6725 2175
Wire Wire Line
	7375 1875 7375 2175
$Comp
L power:GND #PWR0103
U 1 1 5E23A450
P 7375 2450
F 0 "#PWR0103" H 7375 2200 50  0001 C CNN
F 1 "GND" H 7380 2277 50  0000 C CNN
F 2 "" H 7375 2450 50  0001 C CNN
F 3 "" H 7375 2450 50  0001 C CNN
	1    7375 2450
	1    0    0    -1  
$EndComp
Wire Wire Line
	7375 2175 7375 2450
Connection ~ 7375 2175
Wire Wire Line
	4725 1875 4725 1775
$Comp
L Device:R R2
U 1 1 5E23AB20
P 4725 1625
F 0 "R2" H 4795 1671 50  0000 L CNN
F 1 "10k" H 4795 1580 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 4655 1625 50  0001 C CNN
F 3 "~" H 4725 1625 50  0001 C CNN
	1    4725 1625
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR0104
U 1 1 5E23B2F7
P 4725 1475
F 0 "#PWR0104" H 4725 1325 50  0001 C CNN
F 1 "+5V" H 4740 1648 50  0000 C CNN
F 2 "" H 4725 1475 50  0001 C CNN
F 3 "" H 4725 1475 50  0001 C CNN
	1    4725 1475
	1    0    0    -1  
$EndComp
$Comp
L Device:R R1
U 1 1 5E23BBAD
P 4425 1625
F 0 "R1" H 4495 1671 50  0000 L CNN
F 1 "10k" H 4495 1580 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 4355 1625 50  0001 C CNN
F 3 "~" H 4425 1625 50  0001 C CNN
	1    4425 1625
	1    0    0    -1  
$EndComp
Wire Wire Line
	5300 2275 4425 2275
Wire Wire Line
	4425 2275 4425 1775
$Comp
L power:+5V #PWR0105
U 1 1 5E23BF85
P 4425 1475
F 0 "#PWR0105" H 4425 1325 50  0001 C CNN
F 1 "+5V" H 4440 1648 50  0000 C CNN
F 2 "" H 4425 1475 50  0001 C CNN
F 3 "" H 4425 1475 50  0001 C CNN
	1    4425 1475
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR0106
U 1 1 5E23C357
P 6400 875
F 0 "#PWR0106" H 6400 725 50  0001 C CNN
F 1 "+5V" H 6415 1048 50  0000 C CNN
F 2 "" H 6400 875 50  0001 C CNN
F 3 "" H 6400 875 50  0001 C CNN
	1    6400 875 
	1    0    0    -1  
$EndComp
$Comp
L Device:R R3
U 1 1 5E23C365
P 6400 1025
F 0 "R3" H 6470 1071 50  0000 L CNN
F 1 "10k" H 6470 980 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 6330 1025 50  0001 C CNN
F 3 "~" H 6400 1025 50  0001 C CNN
	1    6400 1025
	1    0    0    -1  
$EndComp
$Comp
L Device:C C3
U 1 1 5E23D96C
P 3625 2775
F 0 "C3" V 3373 2775 50  0000 C CNN
F 1 "100nF" V 3464 2775 50  0000 C CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 3663 2625 50  0001 C CNN
F 3 "~" H 3625 2775 50  0001 C CNN
	1    3625 2775
	-1   0    0    1   
$EndComp
$Comp
L Device:C C4
U 1 1 5E23EDCE
P 4050 2775
F 0 "C4" V 3798 2775 50  0000 C CNN
F 1 "4.7uF" V 3889 2775 50  0000 C CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 4088 2625 50  0001 C CNN
F 3 "~" H 4050 2775 50  0001 C CNN
	1    4050 2775
	-1   0    0    1   
$EndComp
Wire Wire Line
	3625 2625 3850 2625
Wire Wire Line
	4050 2925 3850 2925
Wire Wire Line
	3850 2625 3850 2350
Connection ~ 3850 2625
Wire Wire Line
	3850 2625 4050 2625
Text Label 3850 2500 0    50   ~ 0
RTCVCC
$Comp
L power:GND #PWR01
U 1 1 5E23E9D9
P 3850 2925
F 0 "#PWR01" H 3850 2675 50  0001 C CNN
F 1 "GND" H 3855 2752 50  0000 C CNN
F 2 "" H 3850 2925 50  0001 C CNN
F 3 "" H 3850 2925 50  0001 C CNN
	1    3850 2925
	1    0    0    -1  
$EndComp
Connection ~ 3850 2925
Wire Wire Line
	3850 2925 3625 2925
$Comp
L Connector_Generic:Conn_01x01 J2
U 1 1 5E23FAD8
P 1025 2675
F 0 "J2" V 989 2587 50  0000 R CNN
F 1 "Conn_01x01" V 898 2587 50  0000 R CNN
F 2 "MountingHole:MountingHole_2.2mm_M2_ISO14580_Pad" H 1025 2675 50  0001 C CNN
F 3 "~" H 1025 2675 50  0001 C CNN
	1    1025 2675
	0    -1   -1   0   
$EndComp
Wire Wire Line
	1675 2925 1375 2925
Wire Wire Line
	1375 2925 1025 2925
Wire Wire Line
	1025 2925 1025 2875
Connection ~ 1375 2925
Wire Wire Line
	5300 2075 4850 2075
Wire Wire Line
	5300 1975 4850 1975
Text Label 4900 1975 0    50   ~ 0
{slash}IORD
Text Label 4900 2075 0    50   ~ 0
{slash}IOWR
Wire Wire Line
	4725 1875 5300 1875
Wire Wire Line
	4725 2175 4725 1875
Wire Wire Line
	4725 2175 5300 2175
Connection ~ 4725 1875
Text Label 4900 2275 0    50   ~ 0
{slash}RTC_CS
$Comp
L Graphic:SYM_Radioactive_Radiation_Small #SYM1
U 1 1 5E52BB84
P 750 725
F 0 "#SYM1" H 750 992 50  0000 C CNN
F 1 "SYM_Radioactive_Radiation_Small" H 750 600 50  0001 C CNN
F 2 "Bax:baxlogo" H 750 901 50  0000 C CNN
F 3 "~" H 780 525 50  0001 C CNN
	1    750  725 
	1    0    0    -1  
$EndComp
$EndSCHEMATC
